# GrainChainTest

## Introducción

El conocimiento de programación de aplicaciones para móviles ha pasado de manera muy rápida de ser un conocimiento más, a ser una necesitad debido a la rápida implantación y evolución de las plataformas móviles.
Esta rápida evolución crea incertidumbre sobre qué tecnologías son las más adecuadas para la programación de móviles.


## Objetivo

El presente documento tiene como objetivo presentar y explicar en un archivo de texto de fácil acceso el propósito del proyecto, con información sobre cómo instalar, utilizar o también sobre cómo colaborar.


## Requerimientos técnicos

* Versión: Xcode 12.5.1.
* Swift versión: 5
* Cocoapods: >1.10.0
* iOS: > 11.0


## Lineamientos

1. uso exclusivo.


## Requisitos de diseño y experiencia de usuario

Se deberán usar convenciones de estética y funcionamiento basados en este link:
[Apple Guidelines](https://developer.apple.com/ios/human-interface-guidelines/overview/themes/)


## Versionamiento

**git-flow** Es un flujo de trabajo de Git que favorece el desarrollo continuo de software y las prácticas de implementación de DevOps. Fue Vincent Driessen en nvie quien lo publicó por primera vez y quien lo popularizó.
Gitflow es ideal para los proyectos que tienen un ciclo de publicación programado, así como para la práctica recomendada de DevOps de entrega continua.

![gitflow](https://miro.medium.com/max/1276/0*PRJYeVCeztuOuddN.jpg)


## Clases

### Nombre de una clase

Para la codificación en general y en especial para el proyecto se emplea CamelCase para cada clase, en este caso usaremos la siguiente nomenclatura para nombrar a las clases:
DC + NombreClase + TipoClase

Ejemplos:
> DC{NOMBREVIEWCONTROLLER}VC*
> DCMainVC

### Variables

**Nombres de variables:**
Para la codificación en general se empleará lowerCamelCase para cada variable.
*lowerCamelCase:* A veces lo escriben lowerCamelCase, es un estilo de escritura que aplica a un conjunto de palabras, esto se debe a que siempre se escribe con minúscula la primera letra de cada palabra, haciendo que se asemeje a las jorobas de un camello.

Ejemplos:
> var unaVariableNueva


## Consideraciones generales

### Generales

Buenas prácticas
evitar force unwrapping

### Condicionales

No exceder un número de 10 condiciones(anidadas incluyendo las otras estructuras de control y repetición: switch, for, etc) dentro de los condicionales, esto puede llevar a aumentar la complejidad ciclomática de la aplicación.


## Protocolos

Para la definición de protocolos se usará la convención estándar de Apple:

![Protocols](https://developer.apple.com/library/archive/documentation/General/Conceptual/DevPedia-CocoaCore/Art/protocol_2x.png)


## Buenas prácticas

Estas son algunas de las prácticas más comunes que debemos mantener:
* Manténlo simple.
* Declara variables en líneas separadas.
* Explica el propósito de cada clase(documentar).
* Crea principios de clase modificables o extensibles.
* Divide lo complejo en clases simples.
* Mantén pocas líneas por método.
* Utiliza comentarios.
* Documenta funciones.
* Sigue la arquitectura o patrón definido.
* Testea el código.
* No multiplicar código.
* Uso de delimitadores de acceso.


## Arquitectura

VIPER es una adaptación a iOS de la Clean Architecture propuesta por el "gurú" de la ingeniería del software Robert C. Martin, conocido familiarmente como "Uncle Bob". El tío Bob ha explicado los detalles de su clean architecture en múltiples ocasiones, por ejemplo en su blog y en algunas charlas. VIPER aparece descrito por primera vez en este artículo de 2014.

**Componentes de la arquitectura**

- *View:* Muestra los datos que le pasa el presenter, y le pasa a este las acciones del usuario. Como vemos es el mismo papel que desempeña en MVP o MVVM.
- *Interactor:* Es la lógica de negocio, los casos de uso de nuestra aplicación. Típicamente cada módulo VIPER implementará un caso de uso distinto.
- *Presenter:* Contiene la lógica de presentación, al igual que en MVP.
- *Entity:* Los modelos del dominio. Se podría decir que contienen la lógica de negocio "genérica" mientras que el interactor contiene la lógica propia de nuestra aplicación.
- *Router:* Contiene la lógica de navegación, para saber qué pantallas mostrar y cómo cambiar entre ellas. En algunos sitios se conoce también como wireframe.

Los componentes se comunican entre sí según se indica en el siguiente diagrama:
![VIPER](https://koenig-media.raywenderlich.com/uploads/2020/02/viper.png)

**Ventajas e inconvenientes**

Las ventajas de VIPER son las de cualquier arquitectura bien diseñada:
- Facilita la colaboración en el equipo de desarrollo, si cada desarrollador se ocupa de un componente separado o un conjunto de componentes.
- Facilita el mantenimiento de la aplicación.
- Hace posible el testing.

Como inconveniente principal está la sobrecarga que supone crear un mínimo de 5 componentes por cada módulo. Es una arquitectura que para aplicaciones pequeñas o para aplicaciones implementadas por un solo desarrollador quizá presenta una complicación excesiva.
