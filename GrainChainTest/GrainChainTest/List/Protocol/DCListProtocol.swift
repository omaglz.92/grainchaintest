//
//  DCListProtocol.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//  
//
import UIKit
/// Protocolo que rige la vista
protocol DCListViewProtocol: DCViewBaseProtocol {
    /// Función para actualizar la vista
    func updateRoutes()
}
/// Protocolo que rige el presenter
protocol DCListPresenterProtocol {
    /// Función para mostar el detalle de la ruta seleccionada
    /// - Parameter route: Ruta seleccionada
    func showDetail(route: DCRoute)
}
/// Protocolo que rige el router
protocol DCListRouterProtocol {
    /// Función para mostar el detalle de la ruta seleccionada
    /// - Parameter route: Ruta seleccionada
    func showDetail(route: DCRoute)
}

