//
//  DCListRouter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//  
//
import UIKit
final class DCListRouter {
    var view: DCListVC
    private var presenter: DCListPresenter
    init() {
        self.view = DCListVC()
        self.presenter = DCListPresenter()
        view.presenter = self.presenter
        presenter.view = self.view
        presenter.router = self
    }
}
extension DCListRouter: DCListRouterProtocol {
    func showDetail(route: DCRoute) {
        let controller = DCDetailRouter(route: route).view
        let navigation = UINavigationController(rootViewController: controller)
        navigation.modalPresentationStyle = .fullScreen
        view.present(navigation, animated: true, completion: nil)
    }
}

