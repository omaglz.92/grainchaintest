//
//  DCListPresenter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//  
//
import UIKit
final class DCListPresenter: DCListPresenterProtocol {
    var view: DCListViewProtocol?
    var router: DCListRouterProtocol?
    func showDetail(route: DCRoute) {
        router?.showDetail(route: route)
    }
}
