//
//  DCListCell.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//

import UIKit
import CoreLocation

final class DCListCell: UITableViewCell {
    @IBOutlet weak var lblName: UILabel!
    @IBOutlet weak var lblKms: UILabel!
    @IBOutlet weak var lblDate: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    /// Función para configurar los datos en la celda
    /// - Parameter route: Ruta
    func configureCell(route: DCRoute) {
        lblName.text = route.name
        lblKms.text = String(format: "%.3f", routeDistance(route: route)) + " Kms"
        lblDate.text = route.startDate.toDate.getFormattedDate(format: "dd-MMM-yyyy HH:mm")
    }
}
