//
//  DCListVC.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//  
//
import UIKit
final class DCListVC: DCBaseVC {
    @IBOutlet weak private var tableView: UITableView!
    var presenter: DCListPresenter?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTable()
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    /// Función para configurar la tabla
    func configureTable() {
        tableView.register(UINib(nibName: "DCListCell", bundle: nil), forCellReuseIdentifier: "DCListCell")
    }
}
extension DCListVC: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return DCDataManager.shared.routes?.count ?? 0
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let routes = DCDataManager.shared.routes, let cell = tableView.dequeueReusableCell(withIdentifier: "DCListCell", for: indexPath) as? DCListCell else {
            return UITableViewCell()
        }
        cell.configureCell(route: routes[indexPath.row])
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let route = DCDataManager.shared.routes?[indexPath.row] else {
            return
        }
        presenter?.showDetail(route: route)
    }
}
extension DCListVC: DCListViewProtocol {
    func updateRoutes() {
        tableView.reloadData()
    }
}
