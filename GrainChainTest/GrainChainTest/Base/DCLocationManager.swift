//
//  DCLocationManager.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//
import Foundation
import CoreLocation
protocol DCLocationManagerDelegate {
    func didChangeAuthorizationStatus(status: CLAuthorizationStatus)
    func updateLocation(location: CLLocation)
    func firstLocation(location: CLLocation)
}
class DCLocationManager: NSObject {
    var delegate: DCLocationManagerDelegate?
    static var shared: DCLocationManager = {
        let instance = DCLocationManager()
        return instance
    }()
    var timeInterval: TimeInterval = 2.0
    private var locationManager: CLLocationManager = CLLocationManager()
    private var currentLocation: CLLocation?
    private var sendedFirstLocation: Bool = false
    override init() {
        super.init()
        locationManager.delegate = self
        Timer.scheduledTimer(timeInterval: timeInterval, target: self, selector: #selector(shareLocation), userInfo: nil, repeats: true)
    }
    func requestAuthorizationStatus() {
        locationManager = CLLocationManager()
        locationManager.requestAlwaysAuthorization()
        locationManager.startUpdatingLocation()
        locationManager.delegate = self
        locationManager.allowsBackgroundLocationUpdates = true
        locationManager.pausesLocationUpdatesAutomatically = false
    }
    func startUpdatingLocation() {
        locationManager.startUpdatingLocation()
    }
    func stopUpdatingLocation() {
        locationManager.stopUpdatingLocation()
    }
    @objc func shareLocation() {
        guard let currentLocation = currentLocation else {
            return
        }
        delegate?.updateLocation(location: currentLocation)
    }
}
extension DCLocationManager: CLLocationManagerDelegate {
    private func locationManager(manager: CLLocationManager, didChangeAuthorizationStatus status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
        default:
            break
        }
        delegate?.didChangeAuthorizationStatus(status: status)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let location = locations.last else { return }
        if !sendedFirstLocation {
            sendedFirstLocation = true
            delegate?.firstLocation(location: location)
        }
        currentLocation = location
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        print("Errors: " + error.localizedDescription)
    }
}

