//
//  DCBaseVC.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//
import UIKit
import AJMessage
protocol DCViewBaseProtocol {
    func showMessage(message: String, DCType: DCErrorType)
    func showWait()
    func hideWait()
}
class DCBaseVC: UIViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: .plain, target: nil, action: nil)
        self.navigationController?.navigationBar.barTintColor = UIColor.DCprimary
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.tintColor = UIColor.DCItemSelect
        self.navigationController?.navigationBar.titleTextAttributes = [.foregroundColor: UIColor.DCItemSelect]
        setLogo()
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    func setLogo() {
        let imageView = UIImageView(image: UIImage(named: "img_logo"))
        imageView.contentMode = .scaleAspectFit
        imageView.tintColor = UIColor.DCItemSelect
        self.navigationItem.titleView = imageView
    }
    func showMessage(title: String, message: String, DCType: DCErrorType, seconds: Double = 5.0) {
        var type = AJMessage.Status.success
        switch DCType {
        case .success:
            type = AJMessage.Status.success
        case .info:
            type = AJMessage.Status.info
        case .error:
            type = AJMessage.Status.error
        }
        AJMessage.show(title: title, message: message, duration: seconds, position: .top, status: type)
    }
    func showMessage(message: String, DCType: DCErrorType) {
        showMessage(title: "", message: message, DCType: DCType)
    }
    func showWait() {
        DCActityIndicator.shared.showProgress()
    }
    func hideWait() {
        DCActityIndicator.shared.dissmissProgress()
    }
}
enum DCErrorType {
    case success
    case info
    case error
}
