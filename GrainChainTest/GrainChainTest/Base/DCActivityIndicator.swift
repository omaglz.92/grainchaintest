//
//  DCActivityIndicator.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//
import UIKit
final public class DCActityIndicator: NSObject {
    /// Vista transparente para evitar toques en vistas posteriores.
    private var mainView: UIView?
    /// Vista principal contenedora.
    private var viewProgress: UIView?
    /// Vista de carga default del sistrema.
    private var activityIndicator: UIView?
    /// Ancho de la vista de progreso.
    private let widthProgress: CGFloat = 100.0
    /// Ancho de la vista de progreso.
    private let widthActivity: CGFloat = 50.0
    /// Constante de la redondez de borde.
    private let cornerRadius: CGFloat = 15.0
    /// Indicador de progreso.
    private let activity: UIActivityIndicatorView = UIActivityIndicatorView()
    // MARK: - Public Properties
    /// Color efecto fade para progressView
    public var fadeColor: UIColor = UIColor.black.withAlphaComponent(0.3)
    /// Color para progressView
    public var progressColor: UIColor {
        return UIColor.gray
    }
    /// Inicializador del componente
    private override init() {
        super.init()
    }
    /// Instancia única de la clase.
    public static let shared = DCActityIndicator()
    /// Método que construye la vista principal.
    private func buildView() {
        let bounds = UIScreen.main.bounds
        mainView = UIView(frame: bounds)
        let centerX = (bounds.width / 2.0) - (self.widthProgress / 2.0)
        let centerY = (bounds.height / 2.0) - (self.widthProgress / 2.0)
        self.viewProgress = UIView(frame: CGRect(x: centerX, y: centerY, width: self.widthProgress, height: self.widthProgress))
        self.viewProgress?.layer.cornerRadius = self.cornerRadius
        self.mainView?.backgroundColor = .clear
        self.mainView?.addSubview(self.viewProgress!)
        UIApplication.shared.windows.first?.addSubview(self.mainView!)
    }
    /// Método que construye el progress View.
    private func addProgressView() {
        guard let bounds = self.viewProgress?.bounds else { return }
        let centerX = (bounds.width / 2.0) - ((self.widthActivity) / 2.0)
        let centerY = (bounds.height / 2.0) - ((self.widthActivity) / 2.0)
        let frame = CGRect(x: centerX, y: centerY, width: self.widthActivity, height: self.widthActivity)
        self.activityIndicator = UIView(frame: frame)
        self.activity.frame = CGRect(x: 0, y: 0, width: 40, height: 40)
        if let viewProgress = activityIndicator {
            activity.center = CGPoint(x: viewProgress.frame.size.width / 2, y: viewProgress.frame.size.height / 2)
        }
        activity.hidesWhenStopped = true
        if #available(iOS 13.0, *) {
            activity.style = .large
        } else {
            activity.style = .whiteLarge
        }
        self.activityIndicator?.addSubview(activity)
        activity.startAnimating()
        self.viewProgress?.addSubview(self.activityIndicator!)
    }
    /// Función que genera la animación de entrada del progress.
    private func animateShowView() {
        if let progress = self.mainView {
            progress.alpha = 0.0
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.4, initialSpringVelocity: 0.3, options: .curveEaseInOut, animations: {
                progress.backgroundColor = #colorLiteral(red: 0.09803921569, green: 0.09803921569, blue: 0.09803921569, alpha: 0.25)
                progress.alpha = 1.0
            }, completion: nil)
        }
    }
    /// Función que genera la animación de salida del progress.
    private func animateHideView() {
        if let progress = self.mainView {
            //progress.transform = .identity
            progress.alpha = 1.0
            UIView.animate(withDuration: 0.5, delay: 0.0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.3, options: .curveEaseIn, animations: {
                progress.alpha = 0.0
            }, completion: { _ in
                self.dismissViews()
            })
        }
    }
    // MARK: - Public methods
    /// Método que genera un progress inmediatamente.
    public func showProgress() {
        self.showProgress(after: 0.0)
    }
    /**
     Muestra CFProgressView en Pantalla completa con un mensaje dado
     - parameters after: El tiempo de espera en segundos.
     */
    public func showProgress(after: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            if self.viewProgress == nil {
                self.buildView()
                self.addBlurEffect()
                self.addProgressView()
                self.animateShowView()
            }
        }
    }
    /**
     Remueve CFProgressView de Pantalla completa
     */
    public func dissmissProgress() {
        self.dissmissProgress(after: 0.0)
    }
    /// Remueve el progress después de x tiempo.
    /// - Parameter after: Tiempo en segundos.
    public func dissmissProgress(after: Double) {
        DispatchQueue.main.asyncAfter(deadline: .now() + after) {
            if self.viewProgress != nil {
                self.animateHideView()
            }
            self.activity.stopAnimating()
        }
    }
    /// Función que elimina las subvistas.
    fileprivate func dismissViews() {
        self.removeBlurEffect()
        self.activityIndicator?.removeFromSuperview()
        self.activityIndicator = nil
        self.viewProgress?.removeFromSuperview()
        self.viewProgress = nil
        self.mainView?.removeFromSuperview()
        self.mainView = nil
    }
    /// Método que elimina el efecto blur de la vista principal.
    private func removeBlurEffect() {
        if viewProgress?.subviews.count ?? 0 > 0 {
            UIView.animate(withDuration: 0.3) {
                for view in (self.viewProgress?.subviews)! where view is UIVisualEffectView {
                    view.removeFromSuperview()
                    break
                }
            }
        }
    }
    /// Método que añade el efecto blur.
    private func addBlurEffect() {
        let blurEffectView = UIVisualEffectView()
        blurEffectView.frame = (viewProgress?.bounds)!
        blurEffectView.layer.cornerRadius = self.cornerRadius
        blurEffectView.clipsToBounds = true
        UIView.animate(withDuration: 0.5) {
            self.viewProgress?.addSubview(blurEffectView)
            self.viewProgress?.bringSubviewToFront(blurEffectView)
            blurEffectView.effect = UIBlurEffect(style: .light)
        }
    }
}
