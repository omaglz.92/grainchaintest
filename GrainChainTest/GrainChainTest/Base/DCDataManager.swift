//
//  DCDataManager.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//
import Foundation
class DCDataManager: NSObject {
    var fm = FileManager.default
    var subUrl: URL?
    var mainUrl: URL? = Bundle.main.url(forResource: "routes", withExtension: "json")
    var routes: [DCRoute]?
    static var shared: DCDataManager = {
        let instance = DCDataManager()
        return instance
    }()
    override init() {
        super.init()
        getData()
    }
    func saveRoute(route: DCRoute) {
        routes?.append(route)
        saveRoutes()
    }
    func saveRoutes() {
        guard let subUrl = subUrl else {
            return
        }
        writeToFile(location: subUrl)
    }
    func deleteRoute(id: Int) {
        if let index = routes?.firstIndex(where: {$0.id == id}) {
            routes?.remove(at: index)
            self.saveRoutes()
        }
    }
}
extension DCDataManager {
    func getData() {
        do {
            let documentDirectory = try fm.url(for: .documentDirectory, in: .userDomainMask, appropriateFor: nil, create: false)
            subUrl = documentDirectory.appendingPathComponent("routes.json")
            if let mainPath = mainUrl, let subPath = subUrl {
                loadFile(mainPath: mainPath, subPath: subPath)
            }
        } catch {
            print(error)
        }
    }
    func loadFile(mainPath: URL, subPath: URL) {
        if fm.fileExists(atPath: subPath.path) {
            decodeData(pathName: subPath)
            if routes == nil {
                decodeData(pathName: mainPath)
            }
        } else {
            decodeData(pathName: mainPath)
        }
    }
    func decodeData(pathName: URL){
        do {
            let jsonData = try Data(contentsOf: pathName)
            let decoder = JSONDecoder()
            routes = try decoder.decode([DCRoute].self, from: jsonData)
        } catch { }
    }
    func writeToFile(location: URL) {
        do {
            let encoder = JSONEncoder()
            encoder.outputFormatting = .prettyPrinted
            let JsonData = try encoder.encode(routes)
            try JsonData.write(to: location)
        } catch { }
    }
}
/*
 class DCDataManager: NSObject {
 var routes: [DCRoute]?
 static var shared: DCDataManager = {
 let instance = DCDataManager()
 return instance
 }()
 override init() {
 super.init()
 queryRoutes()
 }
 func saveRoute(route: DCRoute) {
 routes?.append(route)
 saveRoutes()
 }
 func saveRoutes() {
 let encoder = JSONEncoder()
 if let encoded = try? encoder.encode(routes) {
 let defaults = UserDefaults.standard
 defaults.set(encoded, forKey: keyUserDefault.routes.rawValue)
 }
 }
 private func queryRoutes() {
 if routes == nil {
 let defaults = UserDefaults.standard
 let decoder = JSONDecoder()
 if let data = defaults.object(forKey: keyUserDefault.routes.rawValue) as? Data,
 let routes = try? decoder.decode([DCRoute].self, from: data) {
 self.routes = routes
 }
 }
 }
 func deleteRoute(id: Int) {
 if let index = routes?.firstIndex(where: {$0.id == id}) {
 routes?.remove(at: index)
 self.saveRoutes()
 }
 }
 func saveCurrentRoute(route: DCRoute) {
 let encoder = JSONEncoder()
 if let encoded = try? encoder.encode(route) {
 let defaults = UserDefaults.standard
 defaults.set(encoded, forKey: keyUserDefault.currentRoute.rawValue)
 defaults.synchronize()
 }
 }
 func getCurrentRoute() -> DCRoute? {
 let defaults = UserDefaults.standard
 if let data = defaults.object(forKey: keyUserDefault.currentRoute.rawValue) as? Data {
 let decoder = JSONDecoder()
 if let routes = try? decoder.decode(DCRoute.self, from: data) {
 return routes
 }
 return nil
 } else {
 return nil
 }
 }
 func deleteCurrentRoute() {
 let defaults = UserDefaults.standard
 defaults.removeObject(forKey: keyUserDefault.currentRoute.rawValue)
 }
 }
 */
enum keyUserDefault: String, CaseIterable {
    case routes
    case currentRoute
}

