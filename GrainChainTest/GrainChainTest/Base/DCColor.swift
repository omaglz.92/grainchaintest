//
//  FileDC.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//
import UIKit
extension UIColor {
    /// Color fondo
    static var DCBackgorund = UIColor(named: "DCBackgorund") ?? .white
    /// Color fondo celdas
    static var DCBackgorundContent = UIColor(named: "DCBackgorundContent") ?? .white
    /// Color item seleccionado
    static var DCItemSelect = UIColor(named: "DCItemSelect") ?? .white
    /// Color item normal
    static var DCItemUnselect = UIColor(named: "DCItemUnselect") ?? .lightGray
    /// Color principal
    static var DCprimary = UIColor(named: "DCprimary") ?? .white
    /// Color texto principal
    static var DCTextPrimary = UIColor(named: "DCTextPrimary") ?? .white
    /// Color texto secundario
    static var DCTextSecondary = UIColor(named: "DCTextSecondary") ?? .black
}
