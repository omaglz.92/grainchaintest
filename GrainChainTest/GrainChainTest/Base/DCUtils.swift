//
//  DCUtils.swift
//  GrainChainTest
//
//  Created by omar hernandez on 28/07/21.
//
import CoreLocation
import Foundation
import GoogleMaps
extension Date {
    /// Convierte a fecha en un entero
    var toInteger: Int {
        let timeInterval = self.timeIntervalSince1970
        let intDate = Int(timeInterval)
        return intDate
    }
    func getFormattedDate(format: String) -> String {
        let dateformat = DateFormatter()
        dateformat.dateFormat = format
        return dateformat.string(from: self)
    }
}
extension Int {
    /// Convierte entero en formato fecha
    var toDate: Date {
        let timeInterval = TimeInterval(self)
        let dateInt = Date(timeIntervalSince1970: timeInterval)
        return dateInt
    }
}
extension UIView {
    ///  Genera una imagen apartir de la vista.
    /// - Returns: La imagen de la vista.
    func generateImage() -> UIImage {
        var image: UIImage = UIImage()
        let renderer = UIGraphicsImageRenderer(size: self.bounds.size)
        image = renderer.image { _ in
            self.drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        }
        return image
    }
}
/// Función para calcular la distancia de una ruta
/// - Parameter route: Routa realizada
/// - Returns: Distancia en KM
func routeDistance(route: DCRoute) -> Double {
    var points1 = route.points
    var points2 = route.points
    if points1.count > 2 {
        points1.removeLast()
        points2.removeFirst()
    }
    var distance = 0.0
    for index in 0...points1.count-1 {
        let point1 = CLLocation(latitude: points1[index].latitude, longitude: points1[index].longitude)
        let point2 = CLLocation(latitude: points2[index].latitude, longitude: points2[index].longitude)
        distance += point1.distance(from: point2)
    }
    return distance/1000
}
/// Función para calcular la duración de una ruta
/// - Parameter route: Ruta realizada
/// - Returns: duración de ruta en cadena de texto HHmmss
func routeDuration(route: DCRoute) -> String {
    guard let endDate = route.endDate?.toDate else { return "error endDate"}
    let startDate = route.startDate.toDate
    let diffComponents = Calendar.current.dateComponents([.hour, .minute, .second], from: startDate, to: endDate)
    var duration: String = ""
    if let hours = diffComponents.hour {
        duration += "\(hours)h "
    }
    if let minutes = diffComponents.minute {
        duration += "\(minutes)m "
    }
    if let seconds = diffComponents.second {
        duration += "\(seconds)s "
    }
    return duration
}
/// Función para trazar un linea en el mapa
/// - Parameter route: Ruta realizada
/// - Returns: línea a pintar
func drawRoute(route: DCRoute) -> GMSMutablePath {
    let path = GMSMutablePath()
    for point in route.points {
        path.add(CLLocationCoordinate2D(latitude: point.latitude, longitude: point.longitude))
    }
    return path
    let polyline = GMSPolyline(path: path)
}
