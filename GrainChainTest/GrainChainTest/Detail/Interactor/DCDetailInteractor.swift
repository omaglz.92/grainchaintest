//
//  DCDetailInteractor.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//  
//
import UIKit
final class DCDetailInteractor: NSObject {
    override init() {
        super.init()
    }
}
extension DCDetailInteractor: DCDetailInteractorInputProtocol {
    func delete(id: Int) {
        DCDataManager.shared.deleteRoute(id: id)
    }
}
