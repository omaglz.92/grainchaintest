//
//  DCDetailRouter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//  
//
import UIKit
final class DCDetailRouter {
    var view: DCDetailVC
    private var presenter: DCDetailPresenter
    private var interactor: DCDetailInteractor
    init() {
        self.view = DCDetailVC()
        self.presenter = DCDetailPresenter()
        self.interactor = DCDetailInteractor()
        view.presenter = self.presenter
        presenter.view = self.view
        presenter.interactor = self.interactor
        presenter.router = self
    }
    convenience init(route: DCRoute) {
        self.init()
        presenter.route = route
    }
}
extension DCDetailRouter: DCDetailRouterProtocol {
    func share(route: DCRoute) {
        let distance: String = String(format: "%.3f", routeDistance(route: route))
        let duration: String = routeDuration(route: route)
        let text: String = "GrainChainTest Datos de ruta \n\nDistancia recorrida: \(distance) Kms\n Duración: \(duration)"
        let activityVC = UIActivityViewController(activityItems: [text], applicationActivities: nil)
        activityVC.excludedActivityTypes = [.postToFacebook,
                                            .postToTwitter,
                                            .postToWeibo,
                                            .print,
                                            .copyToPasteboard,
                                            .assignToContact,
                                            .saveToCameraRoll,
                                            .addToReadingList,
                                            .postToFlickr,
                                            .postToVimeo,
                                            .postToTencentWeibo,
                                            .airDrop,
                                            .openInIBooks,
                                            .markupAsPDF
        ]
        
        if UIDevice.current.userInterfaceIdiom == .pad {
            activityVC.popoverPresentationController?.sourceView = view.view
        }
        view.present(activityVC, animated: true) {
            self.view.hideWait()
        }
    }
}

