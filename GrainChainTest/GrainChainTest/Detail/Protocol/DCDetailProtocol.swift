//
//  DCDetailProtocol.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//  
//
import UIKit
/// Protocolo que rige la vista
protocol DCDetailViewProtocol: DCViewBaseProtocol {
    /// Función para actualizar
    func updateRoutes()
}
/// Protocolo que rige el presenter
protocol DCDetailPresenterProtocol {
    /// Ruta seleccionada
    var route: DCRoute? { get set }
    /// Borra ruta actual
    func delete()
    /// Muesta vista para compartir datos de ruta actual
    func share()
}
/// Protocolo que rige el router
protocol DCDetailRouterProtocol {
    /// Muesta vista para compartir datos de ruta actual
    func share(route: DCRoute)
}
/// Protocolo que rige el interactor
protocol DCDetailInteractorInputProtocol {
    /// Borra ruta actual
    /// - Parameter id: identificador de ruta actual
    func delete(id: Int)
}

