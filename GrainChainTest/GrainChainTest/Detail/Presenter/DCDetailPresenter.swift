//
//  DCDetailPresenter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//  
//
import UIKit
final class DCDetailPresenter: DCDetailPresenterProtocol {
    var view: DCDetailViewProtocol?
    var interactor: DCDetailInteractorInputProtocol?
    var router: DCDetailRouterProtocol?
    var route: DCRoute?
    func delete() {
        guard let routeId = route?.id else {
            return
        }
        view?.showWait()
        interactor?.delete(id: routeId)
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.view?.hideWait()
            self.view?.updateRoutes()
        }
    }
    func share() {
        guard let route = route else { return }
        view?.showWait()
        router?.share(route: route)
    }
}
