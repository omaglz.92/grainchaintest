//
//  DCDetailVC.swift
//  GrainChainTest
//
//  Created by omar hernandez on 29/07/21.
//  
//
import UIKit
import GoogleMaps
final class DCDetailVC: DCBaseVC {
    @IBOutlet weak private var contentMap: UIView!
    @IBOutlet weak private var lblName: UILabel!
    @IBOutlet weak private var lblDistance: UILabel!
    @IBOutlet weak private var lblTime: UILabel!
    @IBOutlet weak private var lblDate: UILabel!
    @IBOutlet weak var stackContet: UIStackView!
    var presenter: DCDetailPresenter?
    var mapView: GMSMapView!
    var camera: GMSCameraPosition!
    var startMarker: GMSMarker?
    var finishMarker: GMSMarker?
    var polyline: GMSPolyline?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap()
        configureNavigation()
        configureData()
        drawRouteOnMap()
        setMarkers()
    }
    /// Función para configurar recursos del mapa
    func configureMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 14.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        self.contentMap.addSubview(mapView)
        polyline = GMSPolyline()
        polyline?.strokeWidth = 5.0
        polyline?.strokeColor = UIColor.blue
        polyline?.geodesic = true
        startMarker = GMSMarker()
        finishMarker = GMSMarker()
    }
    /// Configura barra de navegación
    func configureNavigation() {
        let btnBack = UIBarButtonItem(image: UIImage(named: "ic_back"), style: .done, target: self, action: #selector(closeDetail))
        self.navigationItem.leftBarButtonItem = btnBack
    }
    /// Acción que se activa al presinar el botón de back en la barra de navegación
    @objc private func closeDetail() {
        self.dismiss(animated: true, completion: nil)
    }
    /// Acción que se activa al presinar el botón con icono del botecito de basura
    @IBAction func touchDelete(_ sender: UIButton) {
        presenter?.delete()
    }
    /// Acción que se activa al presinar el botón con icono del compartir
    @IBAction func touchShare(_ sender: UIButton) {
        presenter?.share()
    }
    /// Setea la información en las label
    func configureData() {
        guard let route = presenter?.route else {
            return
        }
        lblName.text = route.name
        lblDistance.text = String(format: "%.3f", routeDistance(route: route)) + " Kms"
        lblTime.text = routeDuration(route: route)
        lblDate.text = route.startDate.toDate.getFormattedDate(format: "dd-MMM-yyyy HH:mm")
    }
    /// Función para pintar ruta en el mapa
    func drawRouteOnMap() {
        guard let route = presenter?.route else {
            return
        }
        polyline?.path = drawRoute(route: route)
        polyline?.map = mapView
        guard let firstLatitude = route.points.first?.latitude,
              let firstLongitude = route.points.first?.longitude,
              let secondLatitude = route.points.last?.latitude,
              let secondLongitude = route.points.last?.longitude
        else { return }
        let firstCoordinate = CLLocationCoordinate2D(latitude: firstLatitude, longitude: firstLongitude)
        let secondCoordinate = CLLocationCoordinate2D(latitude: secondLatitude, longitude: secondLongitude)
        let bounds = GMSCoordinateBounds(coordinate: firstCoordinate, coordinate: secondCoordinate)
        let camera: GMSCameraUpdate = GMSCameraUpdate.fit(bounds)
        mapView.animate(with: camera)
    }
    /// Función para mostar los marcadores en el mapa
    private func setMarkers() {
        guard let route = presenter?.route, let startPoint = route.points.first, let finishPoint = route.points.last else {
            return
        }
        let startPosition = CLLocationCoordinate2D(latitude: startPoint.latitude, longitude: startPoint.longitude)
        let finishPosition = CLLocationCoordinate2D(latitude: finishPoint.latitude, longitude: finishPoint.longitude)
        startMarker = GMSMarker(position: startPosition)
        startMarker?.title = "Inicio"
        startMarker?.icon = GMSMarker.markerImage(with: .green)
        finishMarker = GMSMarker(position: finishPosition)
        finishMarker?.title = "Destino"
        finishMarker?.icon = GMSMarker.markerImage(with: .black)
        startMarker?.map = mapView
        finishMarker?.map = mapView
    }
}
extension DCDetailVC: DCDetailViewProtocol {
    func updateRoutes() {
        closeDetail()
    }
}
