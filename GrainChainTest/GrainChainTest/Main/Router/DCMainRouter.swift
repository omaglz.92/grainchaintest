//
//  DCMainRouter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import UIKit
final class DCMainRouter {
    var view: DCMainVC
    private var presenter: DCMainPresenter
    private var interactor: DCMainInteractor
    init() {
        self.view = DCMainVC()
        self.presenter = DCMainPresenter()
        self.interactor = DCMainInteractor()
        view.presenter = self.presenter
        presenter.view = self.view
        presenter.interactor = self.interactor
        presenter.router = self
    }
}
extension DCMainRouter: DCMainRouterProtocol {
    func showTextfieldAlert() {
        let alert = UIAlertController(title: "Ingresa un nombre para la ruta", message: "", preferredStyle: UIAlertController.Style.alert )
        let save = UIAlertAction(title: "Guardar", style: .default) { [weak self] (alertAction) in
            let textField = alert.textFields![0] as UITextField
            if let text = textField.text, text != "" {
                self?.view.saveRoute(routename: text)
            } else {
                self?.view.showMessage(message: "Ingresa un texto", DCType: .error)
            }
        }
        alert.addTextField { (textField) in
            textField.placeholder = "Nombre de ruta"
        }
        let cancel = UIAlertAction(title: "Cancelar", style: .default) { (alertAction) in }
        alert.addAction(cancel)
        alert.addAction(save)
        view.present(alert, animated: true, completion: nil)
    }
}

