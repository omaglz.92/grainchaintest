//
//  DCMainPresenter.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import UIKit
final class DCMainPresenter: DCMainPresenterProtocol {
    var view: DCMainViewProtocol?
    var interactor: DCMainInteractorInputProtocol?
    var router: DCMainRouterProtocol?
    var currentRoute: DCRoute?
    func showTextfieldAlert() {
        router?.showTextfieldAlert()
    }
    func saveCoordinate(coordinate: DCCoordinate) {
        currentRoute?.points.append(coordinate)
        view?.updateRoute()
    }
    func saveRoute(name: String) {
        view?.showWait()
        currentRoute?.name = name
        currentRoute?.endDate = Date().toInteger
        guard let route = currentRoute else {
            return
        }
        interactor?.saveRoute(route: route)
        currentRoute = nil
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
            self.view?.hideWait()
        }
    }
}
