//
//  DCMainVC.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import UIKit
import GoogleMaps
import CoreLocation
final class DCMainVC: DCBaseVC {
    @IBOutlet weak private var contentMap: UIView!
    @IBOutlet weak private var contentButton: UIView!
    @IBOutlet weak private var imageButton: UIImageView!
    private var isRecord: Bool = false
    var presenter: DCMainPresenter?
    var locationManager = DCLocationManager.shared
    var mapView: GMSMapView!
    var camera: GMSCameraPosition!
    var startMarker: GMSMarker?
    var finishMarker: GMSMarker?
    var polyline: GMSPolyline?
    override func viewDidLoad() {
        super.viewDidLoad()
        configureMap()
        locationManager.delegate = self
        locationManager.requestAuthorizationStatus()
        configureButton()
        NotificationCenter.default.addObserver(self, selector: #selector(saveCurrentRoute), name: Notification.Name("applicationWillTerminate"), object: nil)
    }
    /// Función para configurar recursos del mapa
    func configureMap() {
        let camera = GMSCameraPosition.camera(withLatitude: 0, longitude: 0, zoom: 14.0)
        mapView = GMSMapView.map(withFrame: self.view.frame, camera: camera)
        mapView.isMyLocationEnabled = true
        self.contentMap.addSubview(mapView)
        polyline = GMSPolyline()
        polyline?.strokeWidth = 5.0
        polyline?.strokeColor = UIColor.blue
        polyline?.geodesic = true
        startMarker = GMSMarker()
        finishMarker = GMSMarker()
    }
    /// Función que guarda la ruta en caso de que la app sea cerrada
    @objc private func saveCurrentRoute() {
        let date = Date()
        let dateformat = DateFormatter()
        dateformat.dateFormat = "ddMMyyyyHHmmss"
        let dateString = dateformat.string(from: date)
        saveRoute(routename: "Ruta \(dateString)")
    }
    /// Función que configura el botón de grabar
    private func configureButton() {
        contentButton.layer.cornerRadius = 30.0
        contentButton.layer.shadowColor = UIColor.black.cgColor
        contentButton.layer.shadowOpacity = 0.8
        contentButton.layer.shadowRadius = 3.0
        contentButton.layer.shadowOffset = CGSize(width: 2.0, height: 2.0)
    }
    /// Función que se ejecuta cuando presionas el botón de grabar
    /// - Parameter sender: botón
    @IBAction private func touchButton(_ sender: UIButton) {
        if isRecord {
            presenter?.showTextfieldAlert()
        } else {
            isRecord = true
            imageButton.image = UIImage(named: "ic_stop")
            locationManager.startUpdatingLocation()
            presenter?.currentRoute = DCRoute(id: Date().toInteger, name: nil, points: [], startDate: Date().toInteger, endDate: nil)
        }
    }
    /// Guarda la ruta actual
    /// - Parameter routename: Nombre de la ruta
    func saveRoute(routename: String) {
        locationManager.stopUpdatingLocation()
        imageButton.image = UIImage(named: "ic_play")
        isRecord = false
        presenter?.saveRoute(name: routename)
        mapView.clear()
    }
    /// Dibuja la ruta en el mapa
    private func drawRouteOnMap() {
        guard let currentRute = presenter?.currentRoute else {
            return
        }
        polyline?.path = drawRoute(route: currentRute)
        polyline?.map = mapView
    }
    /// Agrega los marcadores en el mapa
    private func setMarkers() {
        guard let currentRute = presenter?.currentRoute, let startPoint = currentRute.points.first, let finishPoint = currentRute.points.last else {
            return
        }
        let startPosition = CLLocationCoordinate2D(latitude: startPoint.latitude, longitude: startPoint.longitude)
        let finishPosition = CLLocationCoordinate2D(latitude: finishPoint.latitude, longitude: finishPoint.longitude)
        startMarker?.title = "Inicio"
        startMarker?.position = startPosition
        startMarker?.icon = GMSMarker.markerImage(with: .green)
        finishMarker?.position = finishPosition
        finishMarker?.title = "Destino"
        finishMarker?.icon = GMSMarker.markerImage(with: .black)
        startMarker?.map = mapView
        finishMarker?.map = mapView
    }
}
extension DCMainVC: DCMainViewProtocol {
    func updateRoute() {
        drawRouteOnMap()
        setMarkers()
    }
}
extension DCMainVC: DCLocationManagerDelegate {
    func firstLocation(location: CLLocation) {
        camera = GMSCameraPosition.camera(withLatitude: location.coordinate.latitude, longitude: location.coordinate.longitude, zoom: 17.0)
        self.mapView?.animate(to: camera)
    }
    func didChangeAuthorizationStatus(status: CLAuthorizationStatus) {
        switch status {
        case .authorizedAlways, .authorizedWhenInUse:
            locationManager.startUpdatingLocation()
            mapView.isMyLocationEnabled = true
            mapView.settings.myLocationButton = true
        default:
            break
        }
    }
    func updateLocation(location: CLLocation) {
        if !isRecord {
            locationManager.stopUpdatingLocation()
        } else {
            print(location)
            presenter?.saveCoordinate(coordinate: DCCoordinate(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude))
        }
    }
}
