//
//  DCMainInteractor.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import UIKit
final class DCMainInteractor: NSObject {
    private var dataManager = DCDataManager.shared
    override init() {
        super.init()
    }
}
extension DCMainInteractor: DCMainInteractorInputProtocol {
    func saveRoute(route: DCRoute) {
        dataManager.saveRoute(route: route)
    }
}
