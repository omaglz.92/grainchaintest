//
//  DCMainProtocol.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import UIKit
/// Protocolo que rige la vista
protocol DCMainViewProtocol: DCViewBaseProtocol {
    /// Función para actualizar la vista
    func updateRoute()
}
/// Protocolo que rige el presenter
protocol DCMainPresenterProtocol {
    /// Ruta que actualmente se esta grabanado
    var currentRoute: DCRoute? { get set }
    /// Función para mostrar alerta para capturar nombre de la ruta
    func showTextfieldAlert()
    /// Función que agrega un nuevo punto en la ruta actual
    /// - Parameter coordinate: nuevo punto agregado
    func saveCoordinate(coordinate: DCCoordinate)
    /// Función que guarda la ruta actual en el archivo de rutas guardadas
    /// - Parameter name: Nombre de la ruta a guardar
    func saveRoute(name: String)
}
protocol DCMainRouterProtocol {
    /// Función para mostrar alerta para capturar nombre de la ruta
    func showTextfieldAlert()
}
protocol DCMainInteractorInputProtocol {
    /// Función que guarda la ruta actual en el archivo de rutas guardadas
    /// - Parameter route: Ruta a guardar
    func saveRoute(route: DCRoute)
}

