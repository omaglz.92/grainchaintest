//
//  DCMainEntity.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//  
//
import CoreLocation
struct DCRoute: Codable {
    var id: Int
    var name: String?
    var points: [DCCoordinate]
    var startDate: Int
    var endDate: Int?
}
struct DCCoordinate: Codable {
    var latitude: Double
    var longitude: Double
}
