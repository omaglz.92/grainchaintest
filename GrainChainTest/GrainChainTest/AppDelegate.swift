//
//  AppDelegate.swift
//  GrainChainTest
//
//  Created by omar hernandez on 26/07/21.
//
import UIKit
import GoogleMaps
@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        configureMaps()
        window = UIWindow(frame: UIScreen.main.bounds)
        window?.rootViewController = configureMain()
        window!.makeKeyAndVisible()
        return true
    }
    func configureMain() -> UITabBarController {
        /// Mapa
        let mapItem = UITabBarItem()
        mapItem.title = "Mapa"
        mapItem.image = UIImage(named: "ic_map")
        mapItem.selectedImage = UIImage(named: "ic_map")
        let mapVC = DCMainRouter().view
        mapVC.tabBarItem = mapItem
        /// Lista
        let listItem = UITabBarItem()
        listItem.title = "Lista"
        listItem.image = UIImage(named: "ic_list")
        let listVC = DCListRouter().view
        listVC.tabBarItem = listItem
        /// Tab bar
        let tabBarController = UITabBarController()
        tabBarController.tabBar.barTintColor = UIColor.DCprimary
        tabBarController.viewControllers = [mapVC, listVC]
        tabBarController.tabBar.tintColor = UIColor.DCItemSelect
        tabBarController.tabBar.unselectedItemTintColor = UIColor.DCItemUnselect
        return tabBarController
    }
    /// Función que configura la llave de google maps.
    func configureMaps() {
        GMSServices.provideAPIKey("AIzaSyDt2FAcq5POawFhaH4bpQUk4oUXOcDRD1E")
    }
    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }
    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }
    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }
    func applicationWillTerminate(_ application: UIApplication) {
        NotificationCenter.default.post(name: Notification.Name("applicationWillTerminate"), object: nil)
    }
}

